'use strict';
const commander = require('commander');
const fs = require('fs');
const cp = require('child_process');
const template = function (name, item = '') {
    const moduleName = name.split('').map((v, i) => i === 0 ? v.toUpperCase() : v).join('');
    switch (item) {
    case 'controller':
        return `const { Controller, Get, Post } = require('@core/router');\nconst ${moduleName}Service = require('./${name}.service');\n\n@Controller('${name}')\nclass ${moduleName}Controller {\n\tconstructor() {\n\t\tthis.service = new ${moduleName}Service();\n\t}\n}\n\nmodule.exports = ${moduleName}Controller;`;
    case 'model':
        // eslint-disable-next-line no-case-declarations
        return `const mongoose = require('mongoose');\n\nconst ${moduleName}Schema = new mongoose.Schema({\n\n},{});\n\nconst ${moduleName}Model = mongoose.model('${moduleName}', ${moduleName}Schema);\n\nmodule.exports = ${moduleName}Model;`;
    case 'service':
        return `const ${moduleName} = require('./${moduleName}.model');\nconst { Injectable } = require('@core/Provider');\n\n@Injectable()\nclass ${moduleName}Service {\n\tconstructor() { }\n}\n\nmodule.exports = ${moduleName}Service;`;
    }
};

commander
    .version(this.VERSION, '-v, --version')
    .option('-n, --name <name>', 'module name.')
    .parse(process.argv);

const name = commander.name;
const subList = ['controller', 'model', 'service'];
cp.execSync(`mkdir ./src/module/${name}`);
subList.map(item => {
    cp.execSync(`touch ./src/module/${name}/${name}.${item}.js`);
    fs.writeFile(`./src/module/${name}/${name}.${item}.js`, '\'use strict\';\n\n' + template(name, item), () => { console.log(`module ${name} - ${item} 创建完毕`); });
});
