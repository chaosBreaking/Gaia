<h1 align="center">
  Gaia
</h1>

<h3 align="center">The JavaScript implementation of the CapsuleNet.</h3>

<p align="center">
  <a href="https://david-dm.org/ipfs/js-ipfs"><img src="https://david-dm.org/ipfs/js-ipfs.svg?style=flat" /></a>
  <a href="https://github.com/feross/standard"><img src="https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat"></a>
  <a href=""><img src="https://img.shields.io/badge/npm-%3E%3D6.0.0-orange.svg?style=flat" /></a>
  <a href=""><img src="https://img.shields.io/badge/Node.js-%3E%3D10.0.0-orange.svg?style=flat" /></a>
  <br>
</p>

### Project status - `Alpha`

**Want to know what the architecture of CapsuleNet?** Check our [documents folder](/doc).

## Lead Maintainer

[Keii Nimo](https://github.com/chaosBreaking)

[MaxJoseLee](https://github.com/MaxJoseLee)

## Table of Contents

- [Lead Maintainer](#Lead-Maintainer)
- [Table of Contents](#Table-of-Contents)
- [Development](#Development)
  - [Clone and install dependencies](#Clone-and-install-dependencies)
  - [checkout your branch](#checkout-your-branch)
  - [start server use development environment](#start-server-use-development-environment)
  - [Run tests](#Run-tests)
  - [Lint](#Lint)
  - [run web server](#run-web-server)
  - [Code Architecture and folder Structure](#Code-Architecture-and-folder-Structure)
  - [Git commit convention](#Git-commit-convention)
      - [Source code](#Source-code)
  - [Architecture](#Architecture)
- [Contribute](#Contribute)
  - [git flow](#git-flow)
- [License](#License)

## Development

### Clone and install dependencies

```sh
> git clone --depth=1 https://github.com/CapsuleNet/Gaia.git Gaia
> npm i
```

### checkout your branch

```sh
> git checkout -b demo-dev # branch can use any name you want
```

### start server use development environment

```sh
> npm run dev
or
> npm run dev:watch # use nodemon to detect code change and update
```

### Run tests

```sh
> npm run test
```

### Lint

```sh
> npm run lint
```

### run web server

```sh
> npm run start
```

### Code Architecture and folder Structure

./
  index.js 主入口
  baseConfig.js 基本配置项
  doc/ 标准定义文档
  src/
    - core 框架核心模块，包括数据库连接，配置对象，日志模块和依赖注入器等
    - middleware 中间件
    - module 业务模块
    - network 组网模块
    - schedule 定时任务系统
    - utils 工具集

### Git commit convention

- feat：新功能（feature）
- fix：修补bug
- docs：文档（documentation）
- style： 格式（不影响代码运行的变动）
- refactor：重构（即不是新增功能，也不是修改bug的代码变动）
- test：增加测试
- chore：构建过程或辅助工具的变动

##### Source code

### Architecture

## Contribute

### git flow

[your_dev_branch]----pr---->[dev]----pr---->[master/version]

## License