'use strict';

require('@babel/register');
global.mylog = require('@core/logger')({ root: 'log', file: 'server.log' });

const runServer = require('./src/server');
const dbConnect = require('@core/dbConnect');

const run = tasks => {
    [...tasks].reduce((value, func) => {
        if (value instanceof Promise) {
            return value.then(func);
        }
        return func(value);
    }, Promise.resolve(0));
};

run([dbConnect, runServer]);
// run([runServer]);
