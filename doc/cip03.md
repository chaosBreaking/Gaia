# Provider标准
## 简介
Provider称为托管服务提供者，主要提供用户pod的托管服务。
Provider程序需要遵守统一通信格式，包括Provider和客户端通信，Provider之间的通信。
Provider的程序应当开源，防止对用户pod进行不当的操作。
Note: 以下内容中，节点、运行Provider程序的服务器、Provider所代表的意义相同。

## 分类
### 按照具备的功能划分
* FFP 全功能Provider，提供存储、数据分发和区块链服务的节点
* BFP 基本功能Provider 只提供存储和数据分发功能的节点
### 按照所处网络环境划分
* GlobalProvider 处于公网环境
* DomainProvider 域分割，例如US和CN
* LocalProvider 本地环境，例如家庭环境下手机电脑NAS组网

## 功能
* HTTP通信
  * HTTP通信主要用于与客户端的交互。
* UDP/TCP通信
  * 用于Provider之间的通信，一般使用udp，也可以用socket等TCP协议。
* P2P组网
  * CapsuleNet网络主要由Provider组成。
* Pod存储与访问
  * 承载用户Pod的存储功能，响应用户对Pod的CRUD操作，以及从整个CapsuleNet对内容进行查询。
  * 处在公网的Provider可以作为公共托管服务提供者，提供用户在任意环境下对Pod的访问功能。
* 摘录公共资源
  * 将用户的公开数据结构化，可索引化，供外界访问

## 实现方法
### 程序语言
理论上，任何语言的实现都可以作为Provider程序。
作为先行者，我们将使用Javascript/Typescript进行开发，基于其丰富的资源和ES6带来的优良特性，可以很快的推动整个项目的发展。
后续可能会有Go,Rust版本的实现。当然，我们非常欢迎社区以及共同爱好者使用自己喜欢的语言进行开发。
### API约定
#### 与客户端交互
* 创建Pod
  - 约定隐私策略，初始化存储表
  - 约定备份和同步策略，广播至指定节点
* 更新Pod
* 读取Pod
* 删除Pod

#### Provider间通信
* 节点间同步Pod
* 拉取Pod信息

#### 与DDN通信
* 从DDN拉取所有Pod公开数据
* 向DDN推送即时数据