# 场景

Note:

1. 下文中的“用户”表示：一个掌握pod(若干)的实体，因为capsule体系内pod是数据存储和标识其所有者信息的基本单位。
2. 节点，Provider所表达的意思相同。

## Create

1. 客户端生成RSA密钥对{ pubkey, privkey }，使用用户自定义的密码经SHA3-256HASH和AES加密后存储在本地
2. 告知Provider该用户的pubkey，Provider在pod模型下建立一个新的文档，格式如CIP02描述
3. 与用户协商备份策略和同步策略：
     - 备份策略(pod数据在Provider之间的冗余备份)：
       - 是否需要全网备份(capsule网络)
       - 选择可备份Provider
       - 备份频率(day, week, month, year)
     - 同步策略(客户端本地和Provider的pod同步)：
       - 每次修改后，变化部分同步到Provider
       - 按照某个频率定时同步到Provider
       - 手动同步，选择部分或整体同步
     - 伺服

## Update

按照功能分类为存储器和社交网络，分别进行讨论

1. 存储器
     - 上传文件
       - 客户端读取文件源
       - 客户端调用ipfs接口上传到ipfs网络，获得CID
       - 客户端通过统一的资源描述包装CID和文件信息，记录在本地pod内
       - 客户端根据同步策略将这一action同步到Provider
     - 读取/浏览文件
       - 客户端请求Provider，通过给出ipfs的CID或者capsule的uniHash
       - Provider获取内容并返回给客户端
     - 修改文件
       - 客户端将新的文件上传到ipfs后，获得CID
       - 客户端更新本地pod
       - 客户端根据同步策略将这一action同步到provider
     - 删除文件
       - 客户端删除本地pod内容
       - 客户端根据同步策略将这一action同步到provider
     - 下载文件
       - 客户端通过ipfs完成fetch & download
     - 分享文件
       - 客户端获取本文件的ACL策略，将文件的cid和acl策略一并发送到Provider
       - Provider根据本身的服务生成一条分享链接返回给客户端，更新pod
       - 客户端更新本地pod
2. 社交网络
     - 发布信息，包括文章/短信息/评论/回复/引用/多媒体数据
       - 客户端在本地调用ipfs上传文本(富文本)，得到cid后更新本地pod
       - 客户端根据同步策略将这一action同步到Provider
       - Provider根据该数据acl，同步到pod
       - 如果没有特殊的权限控制或者设定公开，则Provider同步到自身缓存下
       - 如果设定了全网发布，则需Provider同步到整个网络
     - 与其他人的关联
        总而言之，在互联网体系内内，用户形成社交关系的步骤可以概括为发现->关联->交互，用户社交的具体行为无非是即时通信和基于内容的互动。
         - 发现：在我们的架构下，IM模型是可以轻松实现的，但是feed模型就需要做一些调整。
         - 关联：关联方式可以从某个纬度分成单向关联和双向关联和群体关联。单向关联的形式有订阅和关注，首先，关注或者订阅需要一个目标。在当前的互联网体系里有以下两种模型进行社交网络构建：
            1. IM模型，类似于wechat, qq等即时通讯软件，两个人要进行通信，必须同时拥有在该体系下的身份，而且需要互换身份。
            2. feed模型，基于服务端的推送，按照传统的中心化架构，所有用户在同一域内，因此可以基于特征进行推荐，基于身份进行搜索。
         - 交互：交互分两种，一种是即时通信，另一种是跟帖/回复模型。
        下文对于具体实现的场景描述全部基于上述社交关系形成和建立以及互动的三方面展开。

## 社交网络构建

### 发现

1. 对等查找

    对等查找类似于QQ和微信里的按照账号查找并添加好友。
    首先，所有用户从整体上来看，是属于capsule网络的。其次，用户在capsule网络中存在的形式，是以pod为基础的，因为pod掌握了数据和对数据拥有者的描述信息。最后，pod分散在各个区域(Provider)里。所以，IM模型的问题处理从两个方面入手，即同一Provider下和不同Provider下。
   1. 同一Provider
      因为在同一个Provider下，所以对于pod的检索就可以寻找到目标pod。
   2. 不同Provider
      对于不在同一个Provider下的用户查找，需要Provider在整个网络节点中进行广播寻找。可以加缓存层进行效率优化，比如为愿意被公共索引和发现的用户建立公共用户表。

2. feed推荐

    feed推荐功能可以由专门的服务来实现，也可以集成在Provider内。
    1. 推荐服务
         - 跨Provider的数据拉取
         - feed流生成
         - 群组/频道feed

    2. Provider集成feed
         - Provider内部内容推送
              - Provider内部需要具备公共内容挖掘和整理功能
         - Provider间内容推送
              - 借助于推荐服务实现

3. 存在的问题

    无论是对等查找还是feed推荐，都面临对于目标的标记和追踪，就是说你需要pin一个id，这个id就是你的交互对象，但是这个id目前来说是pod的公钥或者是可能会实现的类似于区块链的地址，但是这些都是冗长的无意义字符串，**更广泛的用户群体**在实际使用中并不方便。

4. 提出的方案

    我们需要一套类似于dns的命名系统来映射这些无意义长字符串成一些**基于Provider局部作用域的**id，姑且先叫做laid(location area id)。这些laid由两部分组成，第一部分是Provider的标识，起到划分作用域的作用。第二部分是用户自定内容，类似于昵称。Provider分别维护一张映射表和一张路由表，映射表存在于Provider的本地数据存储内，标明了本Provider内的用户和其laid的对应关系。路由表也存储在本地，用于标识其他Provider内的用户。

### 关联

关联大体上可以分成个体间关联和群体关联，其中个体关联可以分成单向关联和双向关联。

1. 单向关联

    常见的单向关联的形式是**推送-订阅/关注**，其核心特征是整个关联体系内数据源(发布源)单一。该形式比较典型的体现是微信公众号文章推送，telegram的channel。

2. 双向关联

    双向关联常见的就是各个IM或者社交软件产品内的好友系统，关系在发起者得到被请求人的同意后就被确定，而且通信双方关系对等。
    从信息的及时性上，可以分成即时和非即时两类，在双向关联的前提下分别举例：
    即时：两个微信用户互相发送/接收信息
    非即时：微博用户发帖/评论/回复

3. 群组关联

    群组关联的核心特征是关联体系内的数据源(发布源)多元化，任何参与此关联的人均可以发布信息。典型的例子是qq和微信群聊，论坛，前者是即时的，后者非即时。

关联只负责如何标定这三种关系。

### 交互

交互是基于关联形式进行的，不同的关联形式就会有不同的交互形式。

- 单向关联

    单向关联的主要实现就是订阅-推送模型
    在线推、离线拉。Provider会将用户发布的公开feed信息推送到全网在线客户端，而离线客户端可以上线后主动拉。客户端维护收发索引，方便推拉。

- 双向关联
  - IM
    - 在同一Provider内的两人通信可以直接基于socket，同时将聊天记录同步到pod内。甚至可以在用户规模扩大时使用现代工业级消息队列架构。
    - 非同一Provider内的通信，需要发起方客户端主动去连接对方Provider，由对方Provider承接IM业务。

    实现基于内容/主题的评论和回复实际上比较简单，在内容/主题所在Provider内建立一个缓存层，存储某pod下内容的互动数据。

- 群组关联

  实现的关键在于跨Provider的多人通信，客户端可以协商至同一个Provider内进行。首先，群组建立于某个特定的Provider，记作PA，某个属于PB的用户加入了PA内的群组后，所有对于PA的通信内容，需要该用户客户端与PA进行交互。

- 拉取feeds流
  - 用户自身的数据直接来自本地或者远程pod
  - 访问其他用户的数据，如果在Provider内检索成功，则缓存并直接返回，否则在p2p网络内查询。

## 实现思想

贯穿整个gaia体系的思想就是插件化、类似微服务架构。

- 按照约定格式存储数据
- 插件根据acl提取数据
- 提取和聚合功能成为独立的服务
