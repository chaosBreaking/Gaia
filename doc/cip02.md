# Pod标准

Pod demo

```json
{
    // [required] 数据所有者公钥
    "owner": "publicKey",
    // [required] pod全局权限控制表
    "acl": {
        /*
            0 public 对外公开
            1 protected 选择性开放，由ACL table决定
            2 private 完全保密
        */
        "deny": 0,
        "allow": 0,
        "namespace": 1,
        "link": 1
    },
    // 全局黑名单
    "deny": ["publicKey1", "publicKey2"], // 黑名单
    // 全局白名单
    "allow": ["publicKey1", "publicKey2"], // 白名单
    // [optional] 类似ipld, Provider提供的用户pod对外地址
    "namespace": ["QmPxebZuW2pgfzj5JWq22KUzxStmqQ6i7YUK9Sq9xepXT9"],
    "type": "master",
    // [required] 存放数据的哈希表
    "container": {
        // 公开数据，Provider及公众可见
        "public": {
            "image": {}, // 图片类
            "media": {}, // 多媒体数据，包括
            "post": {}, // 网络推文
            "link": {}, // 链接类文本，比如跟帖和评论以及回复
            "other": {
                // 哈希表的key是根据cip01定义的capsule网络寻址哈希，value是文件描述对象
                "capsuleUniHash": {
                    // [optional] descriptor 描述该文档，由客户端添加
                    "cid": "/ipfs/QmWGeRAEgtsHW3ec7U4qW2CyVy7eA2mFRVbk1nb24jFyks", // ipfs生成的cid
                    "type": "pdf", // 文件类型
                    "filename": "pdfFile", // 文件名
                    "platform": "ipfs", // 底层存储平台
                    "size": 5242880, // 文件大小(byte)
                },
                "capsuleUniHash": {
                    "cid": "/ipfs/Qmco94dYP733XwrUqFUhDtDG8RsqmGQ6UDPvnmH4Pvy2rv",
                    "type": "doc",
                    "filename": "docFile",
                    "platform": "ipfs",
                    "size": 3160742
                },
            },
        },
        // 受控数据，Provider与白名单公众可见
        "protected": {},
        // 私有数据，Provider与公众不可见
        "private": {},
        ...
    },
    "link": ["/capsule/capsuleUniHash"], // 链接表示对其他Pod内容的引用
    // [optional] 创建时间，由客户端添加
    "createdAt": "ISOTimestamp"
}
```

## 异化Pod

Pod可以进行衍生和继承，可编程化。类似于微服务架构。有一个主Pod，作为用户身份的储存器，同时对于对其他业务Pod进行组织。
例如：

- 主Pod: 主要负责身份认证和组织子类Pod，类似于集群的Master。
  - channelPod: 归属于某个pod，可以被订阅。类似于channel。
  - publicPod: 归属于某个群体，可以按照acl被公开访问和修改，类似于群组。
  - storePod: 归属于个人，专门用于文件存储

不同的Pod对应不同的处理方式，比如channelPod需要作为热点数据，放在Provider的缓存层，并且具备pub/sub功能。publicPod类似于聊天群，具有了IM属性，所以需要IM服务的架构来处理。

尽管Pod可以分化成各种形态，但是具备基本的属性，就是基于ipfs和加密体系，可以自由备份和转移，具有权限控制和私密性。可以根据选择，灵活的权衡安全性和可用性。
