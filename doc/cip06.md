# DDN(Data Deliver Network)标准

收集公共资源，制作成RSS形式、XML和HTML或者JSON/BSON形式，便于分发。

## Feed

feed缓存结构

```javascript
post: {
    data: '文章内容',
    links: {
        link1: {
            data: '评论'
        },
        link2: {
            data: '评论'
            links: {
                data: '评论的评论'
            }
        }
    }
}
```
