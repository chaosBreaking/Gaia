'use strict';
/**
 * Pod是数据存储体
 */
const mongoose = require('mongoose');

const PodSchema = new mongoose.Schema({
    pubkey: {
        type: String,
        unique: true
    },
    address: {
        type: String
    },
    acl: {
        type: {},
        default: {
            deny: 2,
            allow: 2,
            namespace: 2,
            link: 2
        }
    },
    deny: {},
    allow: {},
    namespace: {},
    data: {
        type: {},
        default: {
            public: {},
            private: {},
            protected: {}
        }
    },
    option: {
        type: {},
        default: {}
    }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});

const PodModel = mongoose.model('Pod', PodSchema);

module.exports = PodModel;
