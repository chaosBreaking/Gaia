'use strict';

const { Controller, Get, Post } = require('@core/router');

@Controller('pod')
class PodController {
    // eslint-disable-next-line no-useless-constructor
    constructor (PodService) {
        // 构造函数需要注明所需注入的依赖名，依赖注入器会注入其prototype里
    }

    @Get('query')
    queryPod (req, res, next) {
        const result = this.PodService.queryPod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg,
            data: result.data
        });
    }

    @Post('create')
    createPod (req, res, next) {
        const result = this.PodService.createPod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg,
            data: result.data
        });
    }

    @Post('fetch')
    fetchPod (req, res, next) {
        const result = this.PodService.fetchPod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg,
            data: result.data
        });
    }

    @Post('update')
    updatePod (req, res, next) {
        const result = this.PodService.updatePod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg
        });
    }

    @Post('sync')
    syncPod (req, res, next) {
        const result = this.PodService.updatePod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg
        });
    }

    @Post('backup')
    backupPod (req, res, next) {
        const result = this.PodService.updatePod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg
        });
    }

    @Post('delete')
    deletePod (req, res, next) {
        const result = this.PodService.updatePod(req.body);
        return res.json({
            code: result.code,
            msg: result.msg
        });
    }
}

module.exports = PodController;
