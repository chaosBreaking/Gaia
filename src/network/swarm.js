const { createPeer, startNode } = require('./node.js');
const pull = require('pull-stream');

(async () => {
    const myPeerInfo = await createPeer({});
    const myNode = await startNode(myPeerInfo);
    myNode.handle('/say', (protocol, conn) => {
        pull(
            conn,
            pull.map((v) => v.toString()),
            pull.log(),
        );
    });
    return {
        get peers () {
            return myNode.peerBook;
        },

        get myself () {
            return myPeerInfo;
        },
        dial (target, func, data) {
            return new Promise((resolve, reject) => {
                if (target && target.peerInfo) {
                    myNode.dialProtocol(target.peerInfo, '/say', (err, conn) => {
                        if (err) { reject(err); }
                        pull(pull.values([data]), conn);
                    });
                }
                reject(new Error('Missing required arguments'));
            });
        },
    };
})();
