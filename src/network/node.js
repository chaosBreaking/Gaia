const libp2p = require('libp2p');
const TCP = require('libp2p-tcp');
const defaultsDeep = require('@nodeutils/defaults-deep');
const WebRTCDirect = require('libp2p-webrtc-direct');
const SPDY = require('libp2p-spdy');
const Mplex = require('libp2p-mplex');
const WRTCStar = require('libp2p-webrtc-star');
const wrtc = require('wrtc');
const wrtcstar = new WRTCStar({ wrtc });
// const SECIO = require('libp2p-secio');
const Bootstrap = require('libp2p-railing');
const MulticastDNS = require('libp2p-mdns');
const KadDHT = require('libp2p-kad-dht');

const bootstrapers = [
    '/ip4/104.236.176.52/tcp/4001/p2p-circuit/QmSoLnSGccFuZQJzRadHn95W2CrSFmZuTdDWP8HXaHca9z',
];
class CapsuleProvider extends libp2p {
    constructor (_options) {
        const defaults = {
            modules: {
                transport: [
                    TCP,
                    WebRTCDirect,
                    wrtcstar,
                ],
                streamMuxer: [Mplex, SPDY],
                peerDiscovery: [wrtcstar.discovery, MulticastDNS, Bootstrap],
                dht: KadDHT
                // connEncryption: [SECIO] // if use encryption, cannot receive data...
            },
            config: {
                peerDiscovery: {
                    autoDial: true,
                    webRTCStar: {
                        enabled: true
                    },
                    bootstrap: {
                        interval: 2000,
                        enabled: true,
                        list: bootstrapers
                    },
                    dht: {
                        enabled: true,
                        kBucketSize: 20,
                    },
                }
            }
        };

        super(defaultsDeep(_options, defaults));
    }
}

module.exports = CapsuleProvider;
