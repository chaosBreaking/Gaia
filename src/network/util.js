const PeerInfo = require('peer-info');
const Provider = require('./node.js');

const startNode = peerInfo => new Promise((resolve, reject) => {
    const node = new Provider({ peerInfo });
    node.start().then(() => {
        console.log('node has started (true/false):', node.isStarted());
        console.log('listening on:');
        node.peerInfo.multiaddrs.forEach((ma) => console.log(ma.toString()));
        resolve(node);
    }).catch(err => reject(err));
});

const createPeer = (opts = {}) => new Promise((resolve, reject) => {
    const { id, addrs } = opts;
    PeerInfo.create(id).then(peerInfo => {
        addrs.forEach(addr => peerInfo.multiaddrs.add(addr));
        resolve(peerInfo);
    }).catch(err => reject(err));
});

module.exports = { createPeer, startNode };
