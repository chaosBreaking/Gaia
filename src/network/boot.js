const PeerInfo = require('peer-info');
const Provider = require('./node.js');

PeerInfo.create().then(peerInfo => {
    peerInfo.multiaddrs.add('/ip4/0.0.0.0/tcp/0');
    peerInfo.multiaddrs.add('/ip4/127.0.0.1/tcp/0/http/p2p-webrtc-direct');
    const node = new Provider({ peerInfo: peerInfo });
    node.on('peer:discovery', (peer) => {
        console.log('Discovered:', peer.id.toB58String());
        node.dial(peer, () => {});
    });
    node.on('peer:connect', (peer) => {
        console.log('Connection established to:', peer.id.toB58String());
    });
    node.start().then(() => {
        console.debug('node has started (true/false):', node.isStarted());
        console.debug('listening on:');
        node.peerInfo.multiaddrs.forEach((ma) => console.log(ma.toString()));
    });
});
