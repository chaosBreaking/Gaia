const PeerInfo = require('peer-info');
const Provider = require('./node.js');
const pull = require('pull-stream');

(async () => {
    const createNode = (addrs = []) => new Promise((resolve, reject) => {
        PeerInfo.create().then(peerInfo => {
            addrs.forEach(addr => peerInfo.multiaddrs.add(addr));
            const node = new Provider({ peerInfo: peerInfo });
            node.start().then(() => {
                console.log('node has started (true/false):', node.isStarted());
                console.log('listening on:');
                node.peerInfo.multiaddrs.forEach((ma) => console.log(ma.toString()));
                resolve(node);
            });
        });
    });
    const [node1, node2] = await Promise.all([
        createNode(['/ip4/0.0.0.0/tcp/6057']),
        createNode(['/ip4/0.0.0.0/tcp/6058'])
    ]);
    // Note that pull(a, b, c) is basically the same as a.pipe(b).pipe(c).
    node2.handle('/say', (protocol, conn) => {
        pull(
            conn,
            pull.map((v) => v.toString()),
            pull.log(),
            node2.dialProtocol(node1.peerInfo, '/say', (err, conn) => {
                if (err) { throw err; }
                pull(pull.values(['aloha, i got it!']), conn);
            })
        );
    });
    node1.handle('/say', (protocol, conn) => {
        pull(
            conn,
            pull.map((v) => v.toString()),
            pull.log()
        );
    });
    node1.dialProtocol(node2.peerInfo, '/say', (err, conn) => {
        if (err) { throw err; }
        pull(pull.values([Buffer.from('hello world')]), conn);
    });
})();
