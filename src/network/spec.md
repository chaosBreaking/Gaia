# Transport

according to this [doc](https://github.com/libp2p/interface-transport#modules-that-implement-the-interface), there are seven transport module can be used as transporter

## Implementation

### tcp

[transport compatible] [connection compatible]

JavaScript implementation of the TCP module for libp2p. It exposes the interface-transport for dial/listen. libp2p-tcp is a very thin shim that adds support for dialing to a multiaddr. This small shim will enable libp2p to use other transports.

### webrtc-star

[transport compatible] [connection compatible] [peer discovery compatible]

libp2p WebRTC transport that includes a discovery mechanism provided by the signalling-star

### webrtc-direct

[transport compatible] [connection compatible]

A WebRTC transport built for libp2p (not mandatory to use with libp2p) that doesn't require the set up a signalling server. Caveat, you can only establish Browser to Node.js and Node.js to Node.js connections.

### websocket-star

[transport compatible] [connection compatible] [peer discovery compatible]

libp2p-websocket-star is one of the multiple transports available for libp2p. libp2p-websocket-star incorporates both a transport and a discovery service that is facilitated by the rendezvous server, also available in this repo and module.

### websockets

[transport compatible] [connection compatible]

JavaScript implementation of the WebSockets module that libp2p uses and that implements the interface-transport interface.
need upgrader.

### webrtc-explorer

[transport compatible] [connection compatible]

tl;dr webrtc-explorer is a Chord inspired, P2P Network Routing Overlay designed for the Web platform (browsers), using WebRTC as its layer of transport between peers and WebSockets for the exchange of signalling data (setting up a connection and NAT traversal). Essentially, webrtc-explorer enables your peers (browsers) to communicate between each other without the need to have a server to be the mediator.

## 支持协议

```javascript
Protocols.table = [
  [4, 32, 'ip4'],
  [6, 16, 'tcp'],
  [17, 16, 'udp'],
  [33, 16, 'dccp'],
  [41, 128, 'ip6'],
  [54, V, 'dns4', 'resolvable'],
  [55, V, 'dns6', 'resolvable'],
  [56, V, 'dnsaddr', 'resolvable'],
  [132, 16, 'sctp'],
  // all of the below use varint for size
  [302, 0, 'utp'],
  [421, Protocols.lengthPrefixedVarSize, 'ipfs'],
  [480, 0, 'http'],
  [443, 0, 'https'],
  [460, 0, 'quic'],
  [477, 0, 'ws'],
  [478, 0, 'wss'],
  [479, 0, 'p2p-websocket-star'],
  [275, 0, 'p2p-webrtc-star'],
  [276, 0, 'p2p-webrtc-direct'],
  [290, 0, 'p2p-circuit']
]
```

lib: webRTC used socket.io-client and simple-peer.
