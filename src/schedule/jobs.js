'use strict';

const jobs = [];

function mountJobs (agenda) {
    jobs.map(jobConfig => {
        const { name, processor, type, slot, priority, unique, skipImmediate, timezone } = jobConfig;
        agenda.define(name, {
            timezone,
            skipImmediate,
            priority,
            unique
        }, processor);
        agenda[type](slot, name);
    });
}

module.exports = agenda => mountJobs(agenda);
