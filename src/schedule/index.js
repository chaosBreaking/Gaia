'use strict';
const { inject } = require('.@core/Provider');
const Agenda = require('agenda');
const mongoose = require('mongoose');
const mountJobs = require('./jobs');
const exec = require('child_process').exec;

module.exports = () => inject(async function startJob (SysConfig) {
    const { DB_USER_NAME, DB_PASSWD, DB_HOST, DB_PORT, DB_NAME } = SysConfig;
    mylog.info('连接到任务队列数据库...');
    const jobConn = await mongoose.createConnection(`mongodb://${DB_USER_NAME}:${DB_PASSWD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
        useNewUrlParser: true,
        autoReconnect: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    });
    mylog.info('连接到任务队列数据库成功');
    const agenda = new Agenda({ mongo: jobConn });
    mountJobs(agenda);
    await agenda.start();
    mylog.info('任务队列启动...');
    exec(`npx agendash --db=mongodb://${DB_USER_NAME}:${DB_PASSWD.replace('&', '\\&')}@${DB_HOST}:${DB_PORT}/${DB_NAME} --collection=agendaJobs --port=6057`);
    mylog.info('agendash启动...');
    return agenda;
});
