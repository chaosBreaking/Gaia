const formidable = require('formidable');

module.exports = {
    async parseForm (req, config) {
        if (!req) return {};
        const form = new formidable.IncomingForm();
        Object.assign(form, config);
        form.on('fileBegin', function (name, file) {
            file.path = file.path.replace(/(?<=\/)\S+(?=\.)/, `${config.tag}_${name}`);
        });
        form.onPart = function (part) {
            if (part.filename && (part.mime !== 'image/png' && part.mime !== 'image/jpeg')) return 0;
            form.handlePart(part);
        };
        return new Promise((resolve, reject) => {
            form.parse(req, function (err, fields, data) {
                if (err) return reject({});
                else return resolve({ fields, data });
            });
        });
    }
};
