const jwt = require('jsonwebtoken');

module.exports = {
    createToken: (data = {}, JWT_SECRET, config = {}) => {
        if (typeof config !== 'object') {
            config = {};
        }
        if (!config.maxAge || typeof config.maxAge !== 'number') {
            config.maxAge = 3600;
        }

        const token = jwt.sign({ data }, JWT_SECRET, {
            expiresIn: config.maxAge,
            algorithm: 'HS256'
        });

        return token;
    },
    verifyToken: async (token = '', JWT_SECRET) => {
        return await new Promise(resolve => {
            if (!token) return null;
            jwt.verify(token, JWT_SECRET, (err, res) => {
                if (err) {
                    resolve(null);
                }
                resolve(res);
            });
        });
    }
};
