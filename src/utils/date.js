'use strict';

module.exports = {
    getDate (count) {
        // 拼接时间
        const time1 = new Date();
        const time2 = new Date();
        if (count === 1) {
            time1.setTime(time1.getTime() - (24 * 60 * 60 * 1000));
        } else {
            if (count >= 0) {
                time1.setTime(time1.getTime());
            } else {
                if (count === -2) {
                    time1.setTime(time1.getTime() + (24 * 60 * 60 * 1000) * 2);
                } else {
                    time1.setTime(time1.getTime() + (24 * 60 * 60 * 1000));
                }
            }
        }

        const Y1 = time1.getFullYear();
        const M1 = ((time1.getMonth() + 1) > 9 ? (time1.getMonth() + 1) : '0' + (time1.getMonth() + 1));
        const D1 = (time1.getDate() > 9 ? time1.getDate() : '0' + time1.getDate());
        const timer1 = Y1 + '-' + M1 + '-' + D1 + ' ' + '23:59:59'; // 当前时间

        time2.setTime(time2.getTime() - (24 * 60 * 60 * 1000 * count));
        const Y2 = time2.getFullYear();
        const M2 = ((time2.getMonth() + 1) > 9 ? (time2.getMonth() + 1) : '0' + (time2.getMonth() + 1));
        const D2 = (time2.getDate() > 9 ? time2.getDate() : '0' + time2.getDate());
        const timer2 = Y2 + '-' + M2 + '-' + D2 + ' ' + '00:00:00'; // 之前的7天或者30天
        return [timer2, timer1];
    }
};
