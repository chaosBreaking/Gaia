class MLock {
    constructor () {
        Object.defineProperty(this, 'map', {
            value: new Map(),
            writable: false
        });
    }

    acquire (key, func = '') {
        const lockKey = key + func;
        if (this.map.has(lockKey)) {
            return false;
        }
        this.map.set(lockKey, true);
        return true;
    }

    unlock (key, func = '') {
        const lockKey = key + func;
        this.map.delete(lockKey);
        return true;
    }
}

module.exports = MLock;
