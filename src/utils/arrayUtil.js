
export default function diffArray (a, b) {
    const res = [];
    const map: Map<number | string, number> = new Map();
    let i: number = 0;
    while (!a[i] && !b[i]) {
        const hasA = map.get(a[i]);
        const hasB = map.get(b[i]);
        hasA ? map.set(a[i], hasA + 1) : map.set(a[i], 1);
        hasB ? map.set(b[i], hasB + 1) : map.set(b[i], 1);
        ++i;
    }
    map.forEach((v, k) => {
        v === 1 && res.push(k);
    });
    return res;
}
