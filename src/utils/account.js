'use strict';

const tools = require('./crypto');

const getPath = uid => {
    const pre = Date.now() + '';
    let seed = pre + uid;
    if (seed.length < 19) seed += process.hrtime().join('').slice(seed.length - 19);
    const frag1 = seed.slice(0, 6);
    const frag2 = seed.slice(6, 12);
    const frag3 = seed.slice(-8);
    const append = `/${frag1}'/${frag2}/${frag3}`;
    return {
        BTC: `m/44'/0'${append}`,
        ETH: `m/44'/60'${append}`
    };
};

module.exports = {
    createAccount (type = 'BTC') {
        const keypair = tools.randomKeypair();
        return {
            type,
            ...keypair,
            address: tools.pubkey2address(keypair.pubkey, {
                coin: type
            })
        };
    },
    deriveNewAccount (root, option) {
        const { coin, seed } = option;
        if (!coin || !seed) throw new Error('Invalid Params');
        const path = getPath(seed)[coin];
        const keypair = tools.secword2keypair(root, { coin, path });
        return {
            pubkey: keypair.pubkey,
            address: tools.pubkey2address(keypair.pubkey, { coin }),
            path
        };
    }
};
