'use strict';
const assert = require('assert');
const lib = {
    ipfs: require('./ipfs'),
    bittorrent: 'todo'
};

class Storage {
    constructor (options = {}) {
        const { db, type = 'ipfs', host = '101.132.121.111', port = '5001', protocol = 'http' } = options;
        assert(db, 'db is required');
        assert(type, 'store platform type is required');
        this.store = lib[type]({ host, port, protocol });
        this.type = type;
        this.db = db;
    }

    static getInstance (options = {}) {
        if (!Storage.instance) {
            Storage.instance = new Storage(options);
        }
        return Storage.instance;
    }

    async add (data, options = {}) {
        /*
            ipfs.add(data, [options])
            ipfs.addPullStream([options])
            ipfs.addReadableStream([options])
            ipfs.addFromStream(stream)
            ipfs.addFromFs(path, [options])
            ipfs.addFromURL(url, [options])
        */
        this.store.add(data, options).then(res => {
            this.db.store(res, options);
        });
    }
    async cat () {
        // pod里存的数据，key到底是啥？ cid?还是文件名?或是描述？
        
    }
}

module.exports = Storage;
